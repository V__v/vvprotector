﻿using System.ComponentModel;

namespace VvProtector.Core.Enums
{
  public enum NamespacesRestructuring
  {
    [Description("Не змінювати")]
    AsIs,
    [Description("Винести кожен обфусцуємий тип в один простір імен")]
    EachTypeOneNamespace,
    [Description("Винести усі обфусцуємі типи в один простір імен")]
    AllTypesOneNamespace,
    [Description("Винести усі обфусцуємі типи з просторів імен")]
    NoNamespaces,
  }
}

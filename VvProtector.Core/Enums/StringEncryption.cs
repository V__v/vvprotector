﻿using System.ComponentModel;

namespace VvProtector.Core.Enums
{
  public enum StringEncryption
  {
    [Description("Не криптувати строки")]
    None,
    [Description("Скривати строки")]
    Hide,
    [Description("Скривати і шифрувати строки")]
    Encrypt,
    [Description("Скривати, шифрувати і стискати строки")]
    EnhancedEncrypt,
    [Description("Скривати і шифрувати строки 3DES алгоритмом")]
    Encrypt3Des,
  }
}

﻿using System.ComponentModel;

namespace VvProtector.Core.Enums
{
  public enum Naming
  {
    [Description("Генерує числові імена (0,1,2)")]
    Numbers = 1,
    [Description("Генерує буквенні імена (a,b,c)")]
    Alphabetical = 2,
    [Description("Генерує буквенні і числові імена (a1, b0)")]
    AlphaNumeric = 3,
    [Description("Генерує імена з непечатних символів")]
    NonDisplayable = 4,
  }
}

﻿using NineRays.ILOMD.Options;

namespace VvProtector.Core
{
  public class ObfuscateOptions
  {
    public static ObfuscateOptions Default = new ObfuscateOptions { Members = Enums.Members.Default, Naming = Enums.Naming.AlphaNumeric };

    public Enums.Members Members { get; set; }
    public Enums.Naming Naming { get; set; }
    public Enums.NamespacesRestructuring NamespacesRestructuring { get; set; }
    public Enums.StringEncryption StringEncryptionMode { get; set; }

    internal Options Options
    {
      get
      {
        return new Options
        {
          Members = (Members) Members,          
          Naming = (Naming) Naming,          
          NamespacesRestructuring = (NamespacesRestructuring) NamespacesRestructuring,
          StringEncryptionMode = (StringEncryptionMode) StringEncryptionMode,
        };
      }
    }
  }
}

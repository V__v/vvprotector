print(SegName(ScreenEA()))
start = SegStart(ScreenEA())
end   = SegEnd(ScreenEA())
print('start: '+hex(start))
print('end: '+hex(end))

with open("result_bin", "wb") as f:
    for ea in range(start, end):
        f.write(chr(Byte(ea)))

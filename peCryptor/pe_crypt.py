import os
import pefile
from tornado.template import Template
from os.path import dirname, basename, splitext
import argparse

parser = argparse.ArgumentParser(
    description = '''PECryptor.''',
    formatter_class = argparse.RawTextHelpFormatter
)
parser.add_argument("path", help = '''path to the file.
e.g. pe_crypt.py C:\\putty.exe''')
args = parser.parse_args()
os.chdir(dirname(__file__))


pe = pefile.PE(args.path)
print('*** Adding a new  section...                                       (1 of 7)\n')
pe.add_last_section(size=1024)
print('*** Xor the first section...                                       (2 of 7)\n')
pe.sections[0].xor_data(code=1)
print('*** Saving data from the first section...                          (3 of 7)\n')
pe.data_copy(pe.sections[0].PointerToRawData, pe.sections[-1].PointerToRawData, 512)

asm_goto = Template(open("goto.tpl.asm", "r").read()).generate(
    go=pe.OPTIONAL_HEADER.ImageBase+pe.sections[-1].VirtualAddress+512,
)
with open("goto.asm", "w") as f:
    f.write(asm_goto)
os.system("FASM.EXE goto.asm")

bin_goto = open("goto.bin", "rb").read()
print('*** Replacing data of the first section...                         (4 of 7)\n')
pe.data_replace(offset=pe.sections[0].PointerToRawData, new_data=bin_goto)

copy_from = pe.OPTIONAL_HEADER.ImageBase + pe.sections[-1].VirtualAddress
copy_to   = pe.OPTIONAL_HEADER.ImageBase + pe.sections[0].VirtualAddress
oep       = pe.OPTIONAL_HEADER.ImageBase + pe.OPTIONAL_HEADER.AddressOfEntryPoint
asm_cp_xor = Template(open("copy.tpl.asm", "r").read()).generate(
    copy_from=copy_from,
    copy_to=copy_to,
    copy_len=512,
    xor_len=pe.sections[0].Misc_VirtualSize,
    key_encode=1,
    original_oep=oep
)
with open("copy.asm", "w") as f:
    f.write(asm_cp_xor)
os.system("FASM.EXE copy.asm")

bin_copy = open("copy.bin", "rb").read()
print('*** Adding decryptor code to the new section...                    (5 of 7)\n')
pe.data_replace(offset=pe.sections[-1].PointerToRawData+512, new_data=bin_copy)

print('*** Setting sections characteristics and address of entry point... (6 of 7)\n')
pe.sections[0].Characteristics |= pefile.SECTION_CHARACTERISTICS["IMAGE_SCN_MEM_WRITE"]
pe.OPTIONAL_HEADER.AddressOfEntryPoint = pe.sections[0].VirtualAddress

name_ext = splitext(basename(args.path))
print('*** Creating the result file...                                    (7 of 7)\n')
result_path  = dirname(args.path) + '\\' + name_ext[0] + '_packed' + name_ext[1]
pe.write(filename=result_path)

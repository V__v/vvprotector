﻿using System;
using System.Windows.Forms;

namespace VvProtector.UI
{
  class Program
  {
    [STAThread]
    static void Main()
    {
      Application.Run(new MainForm());
    }
  }
}

﻿namespace VvProtector.UI
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clbMembers = new System.Windows.Forms.CheckedListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbNaming = new System.Windows.Forms.ListBox();
            this.lbNamespacesRestructuring = new System.Windows.Forms.ListBox();
            this.lbStringEncryptionMode = new System.Windows.Forms.ListBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // clbMembers
            // 
            this.clbMembers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.clbMembers.CheckOnClick = true;
            this.clbMembers.FormattingEnabled = true;
            this.clbMembers.HorizontalScrollbar = true;
            this.clbMembers.Location = new System.Drawing.Point(12, 24);
            this.clbMembers.Name = "clbMembers";
            this.clbMembers.Size = new System.Drawing.Size(150, 197);
            this.clbMembers.TabIndex = 2;
            this.clbMembers.SelectedIndexChanged += new System.EventHandler(this.clbMembers_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Елементи:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(321, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Пространства імен:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(477, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Найменування:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(165, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Режим кодування строк:";
            // 
            // lbNaming
            // 
            this.lbNaming.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbNaming.FormattingEnabled = true;
            this.lbNaming.Location = new System.Drawing.Point(480, 24);
            this.lbNaming.Name = "lbNaming";
            this.lbNaming.Size = new System.Drawing.Size(150, 197);
            this.lbNaming.TabIndex = 4;
            this.lbNaming.SelectedIndexChanged += new System.EventHandler(this.lbNaming_SelectedIndexChanged);
            // 
            // lbNamespacesRestructuring
            // 
            this.lbNamespacesRestructuring.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbNamespacesRestructuring.FormattingEnabled = true;
            this.lbNamespacesRestructuring.Location = new System.Drawing.Point(324, 24);
            this.lbNamespacesRestructuring.Name = "lbNamespacesRestructuring";
            this.lbNamespacesRestructuring.Size = new System.Drawing.Size(150, 197);
            this.lbNamespacesRestructuring.TabIndex = 4;
            this.lbNamespacesRestructuring.SelectedIndexChanged += new System.EventHandler(this.lbNamespacesRestructuring_SelectedIndexChanged);
            // 
            // lbStringEncryptionMode
            // 
            this.lbStringEncryptionMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbStringEncryptionMode.FormattingEnabled = true;
            this.lbStringEncryptionMode.Location = new System.Drawing.Point(168, 24);
            this.lbStringEncryptionMode.Name = "lbStringEncryptionMode";
            this.lbStringEncryptionMode.Size = new System.Drawing.Size(150, 197);
            this.lbStringEncryptionMode.TabIndex = 4;
            this.lbStringEncryptionMode.SelectedIndexChanged += new System.EventHandler(this.lbStringEncryptionMode_SelectedIndexChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescription.Location = new System.Drawing.Point(12, 227);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new System.Drawing.Size(621, 42);
            this.txtDescription.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(267, 275);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(108, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Зберегти && Вийти";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 307);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lbNamespacesRestructuring);
            this.Controls.Add(this.lbStringEncryptionMode);
            this.Controls.Add(this.lbNaming);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.clbMembers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "OptionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Опції";
            this.Load += new System.EventHandler(this.OptionsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox clbMembers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListBox lbNaming;
        private System.Windows.Forms.ListBox lbNamespacesRestructuring;
        private System.Windows.Forms.ListBox lbStringEncryptionMode;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button btnSave;

    }
}
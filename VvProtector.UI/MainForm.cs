﻿using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using VvProtector.Core;
using VvProtector.Core.Enums;

namespace VvProtector.UI
{
  public class MainForm : Form
  {
    private OptionsForm _optionsForm;

    #region MainForm

    public MainForm()
    {
      InitializeComponent();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (components != null)
        {
          components.Dispose();
        }
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    #region Controls

    private TextBox txtAssembly;
    private System.Windows.Forms.Label label1;
    private RichTextBox rtbResults;
    private Button btnRun;
    private Button btnOptions;
    private Button btnOpenFileDlg;
    private Container components;

    #endregion

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.txtAssembly = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rtbResults = new System.Windows.Forms.RichTextBox();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnOptions = new System.Windows.Forms.Button();
            this.btnOpenFileDlg = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtAssembly
            // 
            this.txtAssembly.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAssembly.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtAssembly.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAssembly.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssembly.Location = new System.Drawing.Point(117, 8);
            this.txtAssembly.Name = "txtAssembly";
            this.txtAssembly.Size = new System.Drawing.Size(577, 23);
            this.txtAssembly.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Вхідний файл:";
            // 
            // rtbResults
            // 
            this.rtbResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbResults.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbResults.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbResults.Location = new System.Drawing.Point(3, 113);
            this.rtbResults.Name = "rtbResults";
            this.rtbResults.Size = new System.Drawing.Size(731, 252);
            this.rtbResults.TabIndex = 11;
            this.rtbResults.Text = "";
            this.rtbResults.WordWrap = false;
            // 
            // btnRun
            // 
            this.btnRun.AutoSize = true;
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(13, 80);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(79, 27);
            this.btnRun.TabIndex = 13;
            this.btnRun.Text = "Запуск";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnOptions
            // 
            this.btnOptions.AutoSize = true;
            this.btnOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOptions.Location = new System.Drawing.Point(13, 47);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(79, 27);
            this.btnOptions.TabIndex = 14;
            this.btnOptions.Text = "Опції...";
            this.btnOptions.UseVisualStyleBackColor = true;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            // 
            // btnOpenFileDlg
            // 
            this.btnOpenFileDlg.AutoSize = true;
            this.btnOpenFileDlg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenFileDlg.Location = new System.Drawing.Point(700, 8);
            this.btnOpenFileDlg.Name = "btnOpenFileDlg";
            this.btnOpenFileDlg.Size = new System.Drawing.Size(26, 23);
            this.btnOpenFileDlg.TabIndex = 15;
            this.btnOpenFileDlg.Text = "...";
            this.btnOpenFileDlg.UseVisualStyleBackColor = true;
            this.btnOpenFileDlg.Click += new System.EventHandler(this.btnOpenFileDlg_Click);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(738, 366);
            this.Controls.Add(this.btnOpenFileDlg);
            this.Controls.Add(this.btnOptions);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.rtbResults);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAssembly);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Щит";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    #endregion

    private void btnObfuscateIt_Click(object sender, EventArgs e)
    {
      try
      {
        Enabled = false;
        Application.DoEvents();

        Obfuscate();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.ToString());
      }
      finally
      {
        Enabled = true;
        Application.DoEvents();
      }
    }

    private void btnSetOptions_Click(object sender, EventArgs e)
    {
      _optionsForm = new OptionsForm();
      _optionsForm.ShowDialog();
    }

    public string RunPython(string cmd, string args)
    {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = "C:\\Python27\\python.exe";
            start.Arguments = string.Format("\"{0}\" \"{1}\"", cmd, args);
            start.UseShellExecute = false;// Do not use OS shell
            start.CreateNoWindow = true; // We don't need new window
            start.RedirectStandardOutput = true;// Any output, generated by application will be redirected back
            start.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                    string result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                    return result;
                }
            }
    }

    private void Obfuscate()
    {
      if (txtAssembly.Text != string.Empty && File.Exists(txtAssembly.Text))
      {
          try
          {
              // .NET
              System.Reflection.Assembly.LoadFrom(txtAssembly.Text);

              var obfuscatorSettings = GenerateObfuscateSettings();
              var obfuscator = new Obfuscator(obfuscatorSettings);
              MessageBox.Show(obfuscator.Obfuscate() ? "Вдалося!" : "Сталася помилка(", "Результат");
          }
          catch // Native
          {
              Add(RunPython("peCryptor\\pe_crypt.py", txtAssembly.Text));
          }
        
      }
      else
      {
        rtbResults.Text += "Select an assembly" + Environment.NewLine;
      }
    }

    private ObfuscatorSettings GenerateObfuscateSettings()
    {
      var obfuscatorSettings = new ObfuscatorSettings
      {
        CheckConsistencyBeforeObfuscation = false,
        OutputPath = Path.Combine(Path.GetDirectoryName(txtAssembly.Text), "Obfuscated"),
        StripDebugInfo = true,
        StrongNameKeyFilePath = "",
        StrongNameKeyFilePassword = ""
      };
      obfuscatorSettings.AddAssembly(txtAssembly.Text);
      if (_optionsForm != null && _optionsForm.Options != null)
      {
        obfuscatorSettings.ObfuscateOptions = _optionsForm.Options;
      }
      obfuscatorSettings.ObfuscateLoggerSettings = GenerateLoggerSettings();
      return obfuscatorSettings;
    }

    private ObfuscateLoggerSettings GenerateLoggerSettings()
    {
      return new ObfuscateLoggerSettings
      {
        IsRtf = true,
        Clear = Clear,
        Add = Add,
        AddFormatted = AddFormatted,
        Font = rtbResults.Font
      };
    }

    private void Clear()
    {
      rtbResults.Text = string.Empty;
    }

    private void Add(string msg)
    {
      if (string.IsNullOrEmpty(msg)) return;
      rtbResults.Text += msg;
      Application.DoEvents();
    }

    private void AddFormatted(string text, Color textColor)
    {
      Add(text);
      rtbResults.Select(int.MaxValue, 0);
      rtbResults.SelectionColor = textColor;
      rtbResults.SelectedText = text;
      rtbResults.Select(int.MaxValue, 0);
      rtbResults.SelectionColor = rtbResults.ForeColor;
    }

    private void btnRun_Click(object sender, EventArgs e)
    {
        try
        {
            Enabled = false;
            Application.DoEvents();

            Obfuscate();
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.ToString());
        }
        finally
        {
            Enabled = true;
            Application.DoEvents();
        }
    }

    private void btnOptions_Click(object sender, EventArgs e)
    {
        _optionsForm = new OptionsForm();
        _optionsForm.ShowDialog();
    }

    private void btnOpenFileDlg_Click(object sender, EventArgs e)
    {
        var ofd = new OpenFileDialog
        {
            Title = "Виберіть файл для захисту",
            Filter = "Executables(*.exe;*.dll)|*.exe;*.dll|All files (*.*)|*.*",
            FilterIndex = 1,
            InitialDirectory = Directory.GetCurrentDirectory()
        };
        txtAssembly.Text = (ofd.ShowDialog(this) == DialogResult.OK) ? ofd.FileName : string.Empty;
    }

    private void MainForm_Load(object sender, EventArgs e)
    {

    }
  }
}
